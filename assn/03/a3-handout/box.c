/* Name: Nuttapat Koonarangsri
 * ID: 5881043
 */
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "box.h"

void createBox(Box **b, int init_cap) {
    // IMPLEMENT
    if(*b == NULL){
        (*b) = (Box*)calloc(1,sizeof(Box));
        (*b)->cap = init_cap;
        (*b)->size= 0;
        (*b)->data = (int*)calloc(init_cap,sizeof(int));
    }
}

void insert(Box *b, int elem) {
    if(b->size+1 > b->cap){
        int* tmpdata = b->data;
        reallocf(b,(b->cap)*2);
        b->cap *= 2;
        tmpdata = realloc(b->data,(b->cap)*2 * sizeof(int));
        b->data = tmpdata;
    }
    b->data[b->size] = elem;
    b->size++;
}

void removeAll(Box *b, int elem) {
    // IMPLEMENT
    int count=0;
    for(int i=0;i < b->size;i++){
        if(b->data[i] == elem){
            count++;
            memmove(&(b->data[i]),&(b->data[i+1]),(b->size--)*sizeof(int));
            i--;
        }
    }
}

void printBox(Box *b) {
    // IMPLEMENT
    int n = b->size;
    for(int i=0;i<n;i++){
        printf("%d\n",b->data[i]);
    }
}

double getMean(Box *b) {
    // IMPLEMENT
    float sum = 0.0;
    for(int i=0;i<b->size;i++){
        sum += b->data[i];
    }
    return sum/b->size;
}

void dealloc(Box **b) {
    // IMPLEMENT
    if(*b != NULL){
        free((*b)->data);
    }
}

/*
TEST: ./box < test.in
OUTPUT:
AVG = 4.00
box1 size = 10
box2 size = 1
box1 cap = 1024
box2 cap = 4
-- b1
11
2
3
4
5
2
3
2
2
6
-- b2
2
--
-- b1 (after remove)
11
3
4
5
3
6
--
-- b2 (after remove)
--
*/
int main(int argc, char **argv)
{
    Box *b1 = NULL;
    Box *b2 = NULL;

    createBox(&b1, 1);
    createBox(&b2, 2);

    int n;
    scanf("%d", &n);
    for (int i=0; i<n; i++) {
        int tmp;
        scanf("%d",&tmp);
        insert(b1, tmp);
    }

    insert(b2, 2);

    printf("AVG = %0.2f\n",getMean(b1));
    printf("box1 size = %d\n",b1->size);
    printf("box2 size = %d\n",b2->size);
    printf("box1 cap = %d\n",b1->cap);
    printf("box2 cap = %d\n",b2->cap);

    printf("-- b1\n");
    printBox(b1);
    printf("-- b2\n");
    printBox(b2);
    printf("--\n");

    removeAll(b1, 2);
    printf("-- b1 (after remove)\n");
    printBox(b1);
    printf("--\n");
    removeAll(b2, 2);
    printf("-- b2 (after remove)\n");
    printBox(b2);
    printf("--\n");

    dealloc(&b1);
    dealloc(&b2);
}
