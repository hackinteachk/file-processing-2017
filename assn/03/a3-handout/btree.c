/* Name: Nuttapat Koonarangsri
 * ID: 5881043
 */
#include <stdlib.h>
#include <stdio.h>
#include "btree.h"

void insert(Node **tree, int val) {
    // IMPLEMENT
    if(*tree == NULL){
        (*tree) = (Node*)malloc(sizeof(Node));
        (*tree)->left = NULL;
        (*tree)->right = NULL;
        (*tree)->data = val;
        return;
    }
    Node *n = (Node*)malloc(sizeof(Node));
    n->right = NULL;
    n->left = NULL;
    n->data = val;
    // val > root
    if(val > (*tree)->data){
        if((*tree)->right == NULL){
            (*tree)->right = n;
            return;
        }else {
            insert(&(*tree)->right, val);
        }
    }else{
        if((*tree)->left == NULL){
            (*tree)->left = n;
            return;
        }else{
            insert(&(*tree)->left,val);
        }
    }
}

void printHelper(Node* tree, int level){
    if(tree == NULL) return;
    printf("%*s|-%d\n",2*level,"\0",tree->data);
    printHelper(tree->left,level+1);
    printHelper(tree->right,level+1);
}

void print(Node *tree) {
    // IMPLEMENT
    printf("%d\n",tree->data);
    printHelper(tree->left,0);
    printHelper(tree->right,0);
}

void delete(Node *tree) {
    // IMPLEMENT
    if(tree->left != NULL){
        delete(tree->left);
    }
    if(tree->right !=NULL){
        delete(tree->right);
    }
//    printf("deleted %d\n",tree->data);
    free(tree);
}


Node *lookup(Node ** tree, int val) {
    // IMPLEMENT
    if((*tree)==NULL){
        return NULL;
    }
    int d = (*tree)->data;

    if(d == val){
        return (*tree);
    }

    if(val > d){
        return lookup(&((*tree)->right),val);
    }else{
        return lookup(&((*tree)->left),val);
    }
}


/***** Expected output: *****
7
|-2
  |-1
  |-4
|-10
  |-15
    |-12
      |-11
Found
Not Found
 *****************************/
int main(int argc, char **argv)
{
    Node *root = NULL;
    Node *target = NULL;

    // add nodes
    insert(&root, 7);
    insert(&root, 2);
    insert(&root, 4);
    insert(&root, 10);
    insert(&root, 1);
    insert(&root, 15);
    insert(&root, 12);
    insert(&root, 11);

    // Lets see how the tree looks
    print(root);

    // Check if 4 in the tree
    target = lookup(&root, 4);
    if (target) {
        printf("Found\n");
    } else {
        printf("Not Found\n");
    }

    // Check if 44 in the tree
    target = lookup(&root, 44);
    if (target) {
        printf("Found\n");
    } else {
        printf("Not Found\n");
    }

    // Now let's free up the space
    delete(root);
}
