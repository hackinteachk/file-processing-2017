/* Name: Nuttapat Koonarangsri
 * ID: 5881043
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "queue.h"

void push(Queue **q, char *word) {
    // IMPLEMENT
    if((*q) == NULL){
        (*q) = (Queue*)malloc(sizeof(Queue));
        (*q)->head = NULL;
        (*q)->tail = NULL;
    }

    Node *new = (Node*) malloc(sizeof(Node));
    new->data = (char*)malloc(sizeof(char)*strlen(word));
    new->data = strcpy(new->data,word);
    new->next = NULL;

    if((*q)->head == NULL){
        (*q)->head = new;
    }else{
        (*q)->tail->next = new;
    }

    (*q)->tail = new;
//    printf("h: %s t: %s\n",(*q)->head->data,(*q)->tail->data);
}

char *pop(Queue *q) {
    // IMPLEMENT
    if (isEmpty(q)) {
        return NULL;
    }
    Node *tmp = q->head;
    char *s = (char*)malloc(sizeof(char));
    s = strcpy(s,tmp->data);
    q->head = q->head->next;
//    free(tmp);
    return s;

}

void print(Queue *q) {
    // IMPLEMENT
    if(isEmpty(q)){
        printf("No items\n");
    }else{
        Node *temp = q->head;
        while(temp != NULL){
            printf("%s\n",temp->data);
            temp = temp->next;
        }
    }
}

int isEmpty(Queue *q) {
    // IMPLEMENT
    return q == NULL || q->head == NULL || q->tail == NULL;
}

void delete(Queue *q) {
    // IMPLEMENT
    while(!isEmpty(q)){
        Node* tmp = q->head;
        q->head = q->head->next;
        free(tmp);
    }
}

/***** Expected output: *****
No items
a
b
c
a
b
c
d
e
f
No items
s = World
t = Hello
*****************************/
int main(int argc, char **argv)
{
    Queue *q = NULL;

    // print the queue
    print(q);

    // push items
    push(&q, "a");
    push(&q, "b");
    push(&q, "c");
    print(q);
    // pop items
    while (!isEmpty(q)) {
        char *item = pop(q);
        printf("%s\n", item);
        free(item);
    }
    char *item = pop(q);
    assert(item == NULL);

    // push again
    push(&q, "d");
    push(&q, "e");
    push(&q, "f");
    print(q);

    // destroy the queue
    delete(q);
    // print again
    print(q);

    // check copy
    char *s = (char *)malloc(10);
    strcpy(s, "Hello");
    push(&q, s);
    strcpy(s, "World");
    char *t = pop(q);
    printf("s = %s\n", s);
    printf("t = %s\n", t);
    free(t);
    free(s);

    // free the queue
    free(q);
}
