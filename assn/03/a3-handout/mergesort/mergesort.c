/* Name:
 * ID:
 */
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "mergesort.h"

void printEntries(Entry *ent, int n) {
    for (int i = 0; i < n; i++) {
        printf("%d %s\n", (ent + i)->data, (ent + i)->name);
    }
}

void merge(Entry *output, Entry *L, int nL, Entry *R, int nR) {
    // IMPLEMENT
    int i = 0, j = 0, k = 0;
    while (k < nL + nR) {
        if ( ( (L + i) != NULL && i < nL && (L + i)->data < (R + j)->data ) || (R + j) == NULL) {
            *(output + k) = *(L + i);
            i++;
        } else if(j<nR) {
            *(output + k) = *(R + j);
            j++;
        }else{
            *(output + k) = *(L + i);
            i++;
        }
        k++;
    }
}

void merge_sort(Entry *entries, int n) {
    // IMPLEMENT
    if (n > 1) {
        int m = n / 2;
        Entry *tmp = (Entry *) malloc(n * sizeof(Entry));

        Entry *L = entries;
        Entry *R = entries + m;

        merge_sort(L, m);
        merge_sort(R, n-m);

        merge(tmp, L, m, R,n-m);

        memcpy(entries, tmp, n * sizeof(Entry));
        free(tmp);
    }
}

/*
TEST: ./mergesort < test.in
OUTPUT:
1 lsdfjl
2 ghijk
3 ksdljf
5 abc
6 def
*/
int main(void) {
    // IMPLEMENT
    int n;
    scanf("%d", &n);
    Entry *entries = (Entry *) malloc(sizeof(Entry) * n);

    for (int i = 0; i < n; i++) {
        scanf("%d", (&(entries + i)->data));
        (entries + i)->name = (char *) malloc(sizeof(char) * 20);
        scanf("%s", ((entries + i)->name));
    }

    merge_sort(entries, n);

    for (int i = 0; i < n; i++) {
        printf("%d %s\n", (entries + i)->data, (entries + i)->name);
    }


}
