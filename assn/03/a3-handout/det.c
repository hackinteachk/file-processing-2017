#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printMatrix(int **a,int n);

double compute_det(int **a, int n) {
    // implement this
    if(n==1){
        return (double)a[0][0];
    }
    double sum = 0.0;
//  minor of a

    for(int i=0;i<n;i++) {
        int coeff = a[0][i];
        int **min = (int**)malloc((n-1)*(sizeof(int*)));
        int m_r=0;
        for (int r = 1; r < n; r++) {
            min[m_r] = (int*)malloc((n-1)*sizeof(int));
            int m_c=0;
            for (int c = 0; c < n; c++) {
                if (c == i) {
                    continue;
                }
                min[m_r][m_c++] = a[r][c];
            }
            m_r++;
        }
//        printf("cf: %d\n",coeff);
//        printMatrix(min,n-1);
//        printf("\n");
        if(i%2 == 0){
            sum += coeff * compute_det(min,n-1);
//            printf("(+)sum: %.5f\n",sum);
        }else{
            sum -= coeff * compute_det(min,n-1);
//            printf("(-)sum: %.5f\n",sum);
        }
    }
//    printf("sum: %.5f\n",sum);
    return sum;
}

void printMatrix(int **a,int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            printf("%d ",a[i][j]);
        }
        printf("\n");
    }
}

/*
TEST: ./det < det.in
OUTPUT:
-105.00000
*/
int main(void) {
    // implement this
    int n = 0;
    scanf("%d",&n);

    int **a = (int**)malloc(sizeof(int*)*n);
    for(int i=0;i<n;i++){
        a[i] = (int*)malloc(sizeof(int)*n);
        for(int j=0;j<n;j++){
            scanf("%d",&a[i][j]);
        }
    }
    double s = compute_det(a,n);
    printf("%.5f\n",s);
}
