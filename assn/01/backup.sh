# !/bin/bash

p=$1

if [ $# -eq 0 ]
then
    exit 0
fi

d=`date '+%Y-%m-%d_%H:%M:%S'`
p2=$p"_"$d
mkdir -p "/subm/u5881043/backups/$p2"
cp -r $p/* "./backups/$p2"
echo "Backup $p completed successfully."
