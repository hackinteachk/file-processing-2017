#!/bin/bash

path=$1
bpath=`ls backups | grep "test_" | sort -r | head -1`

mkdir -p ./recovered/$path
rm -rf ./recovered/$path
cp -r /subm/u5881043/backups/$bpath/ ./recovered/$path
echo "$path has been restored to ./recovered/$path."
