#include <stdio.h>
#include <ctype.h>

int main(){
    int count[26];
    int max_count = 0;
    char c;

//  Initialize count array
    for(int i=0;i<26;i++){
        count[i] = 0;
    }

    while ((c=getchar()) != EOF){
        if(isalpha(c)){
            int i = tolower(c)-97;
            count[i]++;
            if(count[i] > max_count){
                max_count = count[i];
            }
        }
    }

    for(int i=max_count;i>0;i--){
        for(int j=0;j<26;j++){
            if(count[j] == i){
                printf("*");
                count[j]--;
            }else{
                printf(" ");
            }
        }
        printf("\n");
    }
    printf("abcdefghijklmnopqrstuvwxyz\n");

}
