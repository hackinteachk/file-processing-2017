#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

//replacing every tab occurrence with num_spaces consecutive spaces
void detab(int *n){
    char c;
    while((c=getchar()) != EOF){
        if(c == '\t'){
            printf("%*c",*n,' ');
        }else{
            printf("%c",c);
        }
    }
}


//turn each occurrence of num_spaces consecutive spaces into a tab
void entab(int *n){
    int count;
    char c;
    while((c = getchar()) != EOF){
        //@TODO EDIT
        if(c == ' '){
            count++;
        }else{
            printf("%c",c);
            count = 0;
        }
        if(count == *n){
            printf("\t");
            count = 0;
        }else if(count < *n && count > 0){
            printf("%*c",count,' ');
            count = 0;
        }
    }
    printf("\n");
}

int main(int argc, char **argv){
    if(argc != 3){
        printf("Please run with : tabutil -[flag] <numspace>");
    }

    int n = atoi(argv[2]);
    if(strcmp(argv[1],"-d") == 0){
        detab(&n);
    }
    if(strcmp(argv[1],"-e") == 0){
        entab(&n);
    }
}
