#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

// Defining Area
#define max_row 500000
#define max_length  21

// Tools Declaration
int compare (const void * a, const void * b );
char* sort(char* word);
void printDict(int wc);
int initDict(FILE *dict);
void checkWord(char *jumble);

// Keeping Dictionary ; original_dictinal and Sorted
static char original_dict[max_row][max_length] = {{'\0','\0'}};
static char sorted_dict[max_row][max_length] = {{'\0','\0'}};
static int lengthDict = 0;

// Main
int main(int argc, char* argv[]){

    // Check parameter
    if(argc < 3){
		printf("Invalid Argument\n Usage : unscramble <dictionary> <jumbles>\n");
		exit(-1);
	}

    // each word no more than 20 letters
    char word[max_length];

    // Open file
    FILE *dict = fopen(argv[1], "r");
	FILE *jumb = fopen(argv[2], "r");

    // Initialize dictionary
    lengthDict = initDict(dict);
    // printDict(10);
    // Get each line.
	while(fgets(word,21,jumb)){
        int len = strlen(word);
        if(word[len-1] == '\n'){
            word[len-1] = '\0';
        }
        printf("%s: ",word);
        char* sorted_jumble = sort(word);
        checkWord(sorted_jumble);
        printf("\n");
    }

}

void checkWord(char *jumble){
    int count = 0;
    for(int i=0;i<lengthDict;i++){
        if(strcmp(jumble,sorted_dict[i]) == 0){
            printf("%s ",original_dict[i]);
            count++;
        }
    }
    if(count == 0){
        printf("NO MATCHES");
    }
}

char* sort(char *word){
    int len = strlen(word);
    char* c = word;
    qsort(c,len,sizeof(char),compare);
    return c;
}

void printDict(int wc){
    printf("DICT");
    for(int i=0;i<wc;i++){
        printf("%s\n",original_dict[i]);
    }
    printf("SORTED");
    for(int i=0;i<wc;i++){
        printf("%s\n",sorted_dict[i]);
    }

}

// Initialize dictionary array : original and sorted;
// Return size of dictionary
int initDict(FILE *dict){
    int i=0;
    while(fgets(original_dict[i],max_length,dict)){
        if(original_dict[i][strlen(original_dict[i])-1] == '\n'){
            original_dict[i][strlen(original_dict[i])-1] = '\0';
        }
        char c[21];
        strcpy(c,original_dict[i]);
        char* s = sort(c);
        strcpy(sorted_dict[i],s);
        i++;
    }
    return i;
}


int compare (const void * a, const void * b ){
  return *(char*)a - *(char*)b;
}
