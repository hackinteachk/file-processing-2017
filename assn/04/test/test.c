#include <stdio.h>

int allEvenBits(int x) {
  return x&(x>>16)&(x>>8)&(x>>4)&(x>>2)&1;
}

/*
 * implication - return x -> y in propositional logic - 0 for false, 1
 * for true
 *   Example: implication(1,1) = 1
 *            implication(1,0) = 0
 *   Legal ops: ! ~ ^ |
 *   Max ops: 5
 *   Rating: 2
 */
int implication(int x, int y) {
    return !x | y;
}

int logicalShift(int x, int n) {
  return (x>>n)& ~(((0x1 << 31) >> n) << 1);
}

int tmin(void) {
  return (0x1 << sizeof(int)*8);
}

int byteSwap(int x, int n, int m) {
    int mask = ( (x>>(n << 3)) ^ (x >> (m<<3)) ) & 255;
    return (x ^ (mask << (n<<3))) ^ (mask << (m<<3));
}

int main(){
    // int a1 = allEvenBits(0xFFFFFFFE);
    // int a2 = allEvenBits(0x55555);
    // printf("%d\n",a2);
    //
    // int i1 = implication(1,1);
    // int i2 = implication(0,1);
    // int i3 = implication(0,0);
    // int i4 = implication(1,0);
    // printf("i1: %d, i2: %d\n",i1,i2);
    // printf("i3: %d, i4: %d\n",i3,i4);

    // LOGICAL shift
    // printf("%d\n",0x1);
    // printf("%x\n",logicalShift(0x87654321,3));

    //Byte swaps
    int s = byteSwap(0xDEADBEEF,0,2);
    printf("%d\n",0xDEADBEEF);
}
