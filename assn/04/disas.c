#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void print_hex(const unsigned char *ch, int n){
    for(int i=0;i<n;i++){
        printf("%02x ",ch[i]);
    }
}

void disassemble(const unsigned char *raw_instr){

    const char* map[] = {"rax","rcx","rdx","rbx","rsp","rbp","rsi","rdi"};
    if(raw_instr[0] == 0xff){
        if((raw_instr[1] == 0x74)){
            print_hex(raw_instr,4);
            unsigned char scaled = 1 << (raw_instr[2] >> 6);
            printf("pushq %#x(%s, %s, %d)\n",raw_instr[3],map[raw_instr[2]&0x3],map[(raw_instr[2]>>3)&0x3],scaled);
            return;
        }
        if((raw_instr[1] == 0x70)){
            //case 3 bytes
            print_hex(raw_instr,3);
            int i = raw_instr[1]-(raw_instr[1]&0x70);
            printf("pushq %#x(%c%s)\n",raw_instr[2],'%',map[i]);
            return;
        }
        if((raw_instr[1]&0x70) == 0x30){
            // case 2 bytes
            print_hex(raw_instr,2);
            int i = raw_instr[1]-(raw_instr[1]&0x30);
            printf("pushq (%c%s)\n",'%',map[i]);
            return;
        }
    }

    if((raw_instr[0]&0x50) == 0x50){
        print_hex(raw_instr,1);
        int i = raw_instr[0]-(raw_instr[0]&0x50);
        printf("pushq %c%s\n",'%',map[i]);
        return;
    }

    if(raw_instr[0] == 0x68){
        print_hex(raw_instr,5);
        printf("pushq $0x");
        int count = 4;
        while(raw_instr[count] == 0){
            count--;
        }
        while(count>0){
            printf("%x",raw_instr[count--]);
        }
        printf("\n");
        return;
    }
}
