/*
* Name : Nuttapat Koonarangsri
* ID : 5881043
*/
/*
 * trans.c - Matrix transpose B = A^T
 *
 * Each transpose function must have a prototype of the form:
 * void trans(int M, int N, int A[N][M], int B[M][N]);
 *
 * A transpose function is evaluated by counting the number of misses
 * on a 1KB direct mapped cache with a block size of 32 bytes.
 */
#include <stdio.h>
#include "cachelab.h"

void trans_32x32(int M, int N, int A[N][M], int B[M][N]);
void trans_64x64(int M, int N, int A[N][M], int B[M][N]);
void trans_61x67(int M, int N, int A[N][M], int B[M][N]);
int is_transpose(int M, int N, int A[N][M], int B[M][N]);

/*
 * transpose_submit - This is the solution transpose function that you
 *     will be graded on for Part B of the assignment. Do not change
 *     the description string "Transpose submission", as the driver
 *     searches for that string to identify the transpose function to
 *     be graded.
 */
char transpose_submit_desc[] = "Transpose submission";
void transpose_submit(int M, int N, int A[N][M], int B[M][N])
{
    if(M==32){
        trans_32x32(M,N,A,B);
    }
    if(M==64){
        trans_64x64(M,N,A,B);
    }
    if(M==61){
        trans_61x67(M,N,A,B);
    }
}

/*
 * You can define additional transpose functions below. We've defined
 * a simple one below to help you get started.
 */

void trans_32x32(int M, int N, int A[N][M], int B[M][N]){
    for(int i=0;i<M;i += 8){
        for(int j=0;j<N;j+=8){
            int p,q;
            for(p=i+7;(p>(i+3)) && p<M; p--){
                for(q = j+3; (q>=j) && q<N;q++){
                    B[q][p] = A[p][q];
                }
            }

            for(p=i+7;(p>(i+3))&&p<M;p--){
                for(q=j+4;(q<=(j+7))&& q<N;q++){
                    B[q-4][p-4] = A[p][q];
                }
            }

            for(p=i;(p<=(i+3)) && p<M;p++){
                for(q=j+4; (q<= (j+7)) &&q<N;q++){
                    B[q][p] = A[p][q];
                }
            }

            for(p = i; (p<=(i+3))&& p<M;p++){
                for(q=j; (q<=(j+3))&&q<N;q++){
                    B[q+4][p+4] = A[p][q];
                }
            }

            for(p=i; (p<=(i+3)) && p<M;p++){
                for(q=j; (q<=(j+3)) && q<N;q++){
                    int tmp = B[p+4+(j-i)][q+4+(i-j)];
                    B[p+4+(j-i)][q+4+(i-j)] = B[p+(j-i)][q+(i-j)];
                    B[p+(j-i)][q+(i-j)] = tmp;
                }
            }
        }
    }
}

void trans_64x64(int M, int N, int A[N][M], int B[M][N]){
    int tmp;
    int p,q;
    for(int i=0;i<M;i+=8){
        for(int j=0;j<N;j+=8){
            int dx = j-i;
            int dy = i-j;

            for(p = i; p<(i+2); p++){
                for(q=j; q<(j+8);q++){
                    B[p+2+dx][q+dy] = A[p][q];
                }
            }

            for(p = i+dx; p<(j+4+dx);p++){
                for(q=j+dy;q<(j+4+dy);q++){
                    if((q-(j+dy)) < (p-(i+dx))){
                        int tmp = B[p][q];
                        B[p][q] = B[q+dx][p+dy];
                        B[q+dy][p+dy] = tmp;
                    }
                }
            }

            for(p = i+dx;p<(i+4+dx);p++){
                for(q=j+4+dy; q<(j+8+dy); q++){
                    if(q-(j+4+dy) < (p-(i+dx))){
                        int tmp = B[p][q];
                        B[p][q] = B[q+dx-4][p+dy+4];
                        B[q+dx-4][p+dy+4] = tmp;
                    }
                }
            }

            for(p=i; p<(i+2); p++){
                for(q=j+4; q<(j+8);q++){
                    tmp = B[p+dx][q+dy];
                    B[p+dx][q+dy] = B[p+2+dx][q+dy];
                    B[p+2+dx][q+dy] = tmp;
                }
            }

            for(p=i+4; p<(i+6); p++){
                for(q=j; q<(j+8); q++){
                    B[p+2+dx][q+dy] = A[p][q];
                }
            }

            for(p=i+6; p<(i+8); p++){
                for(q=j;q<(j+8); q++){
                    B[p-2+dx][q+dy] = A[p][q];
                }
            }

            for(p=i+4; p<(i+6); p++){
                for(q=j;q<(j+8);q++){
                    tmp = B[p+dx][q+dy];
                    B[p+dx][q+dy] = B[p+2+dx][q+dy];
                    B[p+2+dx][q+dy] = tmp;
                }
            }

            for(p=i+4+dx; p<(i+8+dx); p++){
                for(q=j+dy; q<(j+4+dy); q++){
                    if((q-(j+dy)) < (p-(i+4+dx))){
                        tmp=B[p][q];
                        B[p][q]=B[q+dx+4][p+dy-4];
                        B[q+dx+4][p+dy-4]=tmp;
                    }
                }
            }

            for(p = i+4+dx; p<(i+8+dx);p++){
                for(q=j+4+dy;q<(j+8+dy);q++){
                    if((q-(j+4+dy)) < (p-(i+4+dx))){
                        tmp = B[p][q];
                        B[p][q] = B[q+dx][p+dy];
                        B[q+dx][p+dy] = tmp;
                    }
                }
            }

            for(p=i+4; p<(i+6);p++){
                for(q=j;q<(j+4);q++){
                    tmp = B[p+dx][q+dy];
                    B[p+dx][q+dy] = B[p+2+dx][q+dy];
                    B[p+2+dx][q+dy] = tmp;
                }
            }

            for(p=i+4; p<(i+6);p++){
                for(q=j;q<(j+4);q++){
                    tmp = B[p+dx][q+dy];
                    B[p+dx][q+dy] = B[p-2+dx][q+4+dy];
                    B[p-2+dx][q+4+dy] = tmp;
                }
            }

            for(p=i+6; p<(i+8); p++){
                for(q = j; q<(j+4);q++){
                    tmp = B[p+dx][q+dy];
                    B[p+dx][q+dy] = B[p-6+dx][q+4+dy];
                    B[p-6+dx][q+4+dy] = tmp;
                }
            }
        }
    }
}

void trans_61x67(int M, int N, int A[N][M], int B[M][N]){
    int p,q;
    int tmp;
    for(int i=0;i<M;i+=18){
        for(int j=0;j<N;j+=18){
            for(p = i; (p<(i+18)) && (p<M); p++){
                for(q=j;(q<(j+18)) && q<N;q++){
                    if(p == q){
                        tmp = A[p][p];
                    }else{
                        B[p][q] = A[q][p];
                    }
                }

                if(i==j){
                    B[p][p] = tmp;
                }
            }
        }
    }
}
/*
 * trans - A simple baseline transpose function, not optimized for the cache.
 */
char trans_desc[] = "Simple row-wise scan transpose";
void trans(int M, int N, int A[N][M], int B[M][N])
{
    int i, j, tmp;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; j++) {
            tmp = A[i][j];
            B[j][i] = tmp;
        }
    }

}

/*
 * registerFunctions - This function registers your transpose
 *     functions with the driver.  At runtime, the driver will
 *     evaluate each of the registered functions and summarize their
 *     performance. This is a handy way to experiment with different
 *     transpose strategies.
 */
void registerFunctions()
{
    /* Register your solution function */
    registerTransFunction(transpose_submit, transpose_submit_desc);

    /* Register any additional transpose functions */
    registerTransFunction(trans, trans_desc);

}

/*
 * is_transpose - This helper function checks if B is the transpose of
 *     A. You can check the correctness of your transpose by calling
 *     it before returning from the transpose function.
 */
int is_transpose(int M, int N, int A[N][M], int B[M][N])
{
    int i, j;

    for (i = 0; i < N; i++) {
        for (j = 0; j < M; ++j) {
            if (A[i][j] != B[j][i]) {
                return 0;
            }
        }
    }
    return 1;
}
