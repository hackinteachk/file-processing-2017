// Name : Nuttapat Koonarangsri
// ID : 5881043

#include "cachelab.h"

#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>

#define HIT 1
#define MISS 2
#define MISS_N_HIT 3
#define MISS_EVICTION_HIT 4
#define MISS_EVICTION 5

typedef struct
{
    int valid;
    int lru; //least recent used
    unsigned int tag;
}Line;

typedef struct
{
    Line *lines;
}Set;

typedef struct
{
    int num_set; // # set S
    int num_line; // # lines
    Set *sets; // pointer to sets
}Cache;

int hit = 0;
int miss = 0;
int evict = 0;
int h_flag = 0;
int v_flag = 0;
int b = 0;
int e = 0;
int s = 0;
char *t = NULL;
int setMask = 0;
int tagMask = 0;

void printUsage(char* argv[]);
int get_tag(int addr, int s, int b);
int get_set(int addr, int s, int b);
void set_lru(Cache *c, int set, int line);
void print(Cache * cache);
int read_line(Cache *c, int s, int e, int b, int addr, char id, int size);

int main(int argc, char *argv[]) {
    int arg = 0;
    int s=0,e=0,b=0;

    while((arg = getopt(argc,argv,"hvs:E:b:t:")) != -1){
        switch(arg){
            case 'h':
                h_flag = 1;
                break;
            case 'v':
                v_flag = 1;
                break;
            case 'b':
                b = atoi(optarg);
                break;
            case 's':
                s = atoi(optarg);
                break;
            case 'E':
                e = atoi(optarg);
            case 't':
                t = optarg;
                break;
            case '?':
                fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                return 1;
            default:
                abort(); //didnt get anything, abort the prog
//            default:
//                printUsage(argv);
        }
    }

    printf("hflag: %d vflag: %d svalue: %d evalue: %d tvalue: %s bvalue: %d\n", h_flag, v_flag, s, e, t, b);

    Cache *cache = NULL;

    cache = (Cache*)malloc(sizeof(Cache));
    cache -> num_set = (2<<s);
    cache -> num_line = e;
    cache -> sets = (Set*) malloc(cache->num_set * sizeof(Set));

    for(int i=0;i<cache->num_set;i++){
        cache ->sets[i].lines = malloc(cache->num_line*sizeof(Line));
        for(int j=0;j<cache->num_line;j++){
            cache->sets[i].lines[j].valid = 0;
            cache->sets[i].lines[j].lru = 0;
            cache->sets[i].lines[j].tag = 0;
        }
    }

    FILE *tracefile;

    tracefile = fopen(t,"r");
    char id;
    unsigned addr;
    int size;

    while(fscanf(tracefile, " %c %x,%d",&id,&addr,&size) > 0){
        if(id != 0x49){
            int linestatus = read_line(cache,s,e,b,addr,id,size);

            switch(linestatus) {
                case HIT:
                    printf("%c, %x,%d hit\n", id, addr, size);
                    break;
                case MISS_N_HIT:
                    printf("%c, %x,%d miss and hit\n", id, addr, size);
                    break;
                case MISS_EVICTION_HIT:
                    printf("%c, %x,%d miss eviction hit", id, addr, size);
                    break;
                case MISS_EVICTION:
                    printf("%c, %x,%d miss eviction\n", id, addr, size);
                    break;
                case MISS:
                    printf("%c, %x,%d miss\n", id, addr, size);
                    break;
            }
        }
    }

    fclose(tracefile);

    free(cache);

    printSummary(hit, miss, evict);
    return 0;
}

void printUsage(char* argv[])
{
    printf("Usage: %s [-hv] -s <num> -E <num> -b <num> -t <file>\n", argv[0]);
    printf("Options:\n");
    printf("  -h         Print this help message.\n");
    printf("  -v         Optional verbose flag.\n");
    printf("  -s <num>   Number of set index bits.\n");
    printf("  -E <num>   Number of lines per set.\n");
    printf("  -b <num>   Number of block offset bits.\n");
    printf("  -t <file>  Trace file.\n");
    printf("\nExamples:\n");
    printf("  %s -s 4 -E 1 -b 4 -t traces/yi.trace\n", argv[0]);
    printf("  %s -v -s 8 -E 2 -b 4 -t traces/yi.trace\n", argv[0]);
    exit(0);
}

int get_tag(int addr, int s, int b){
    int mask =0;
    mask = 0x7fffffff >> (31-s-b);
    addr = addr >> (s+b);
    return (mask&addr);
}

int get_set(int addr, int s, int b){
    int mask = 0;
    mask = 0x7ffffff >> (31-s);
    addr = addr >> b;
    return (mask&addr);
}

void set_lru(Cache *c, int set, int line){
//    most recent = 0, least recent used 0
    for(int i=0;i<c->num_line; ++i){
        if(c->sets[set].lines[i].valid == 1 &&
                c->sets[set].lines[i].lru > c->sets[set].lines[line].lru ){
            (c -> sets[set].lines[i].lru)--;
        }
    }

    c -> sets[set].lines[line].lru = c->num_line;
}

int read_line(Cache *c, int s, int e, int b, int addr, char id, int size){
    int setbit = get_set(addr,s,b);
    int tagbit = get_tag(addr,s,b);
    for(int i=0;i<c->num_line;++i){
        // valid and tag matches
        if(c->sets[setbit].lines[i].valid == 1 &&
                c->sets[setbit].lines[i].tag == tagbit){
            if(id == 'M'){
                hit += 2;
            }else{
                hit++;
            }
            set_lru(c,setbit,i);
            return HIT;
        }
    }

    ++miss;

    for(int i=0;i<c->num_line;i++){
        // not valid
        if(c->sets[setbit].lines[i].valid == 0){
            c->sets[setbit].lines[i].tag = tagbit;
            c->sets[setbit].lines[i].valid = 1; // initialize cache line
            set_lru(c,setbit,i);
            if(id == 'M'){
                hit++;
                return MISS_N_HIT;
            }else{}
                return MISS;
        }
    }

    evict++;
    for(int i=0;i<c->num_line;i++){
        if(c->sets[setbit].lines[i].lru == 1){
            c->sets[setbit].lines[i].valid = 1;
            c->sets[setbit].lines[i].tag = tagbit;
            set_lru(c,setbit,i);
            if(id=='M'){
                hit++;
                return MISS_EVICTION_HIT;
            }else{
                return MISS_EVICTION;
            }
        }
    }
    return 0;
}