#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <strings.h>
#include "cachelab.h"

#include <math.h>

// 64 bit variable for keeping address
typedef unsigned long long int mem_addr_t;

typedef struct {
    // valid bit
    int valid;
    // count last used line
    int access_count;
    mem_addr_t tag;
    // block
    char *block;
} line;

typedef struct {
    // E numbers of lines
    line *lines;
} cache_set;

// main cache struct
typedef struct {
    cache_set *sets;
} cache;


typedef struct {
    int s; // 2**s cache sets
    int b; // block size 2**b bytes per line
    int E; // cachelines per cache_set
    int S; // number of sets, S = 2**s
    int B; // line block size (bytes), B = 2**b

    int hit;
    int miss;
    int evct;
} var;

int v_tag;

/*
 * printUsage - Print usage info
 */
void printUsage(char* argv[])
{
    printf("Usage: %s [-hv] -s <num> -E <num> -b <num> -t <file>\n", argv[0]);
    printf("Options:\n");
    printf("  -h         Print this help message.\n");
    printf("  -v         Optional verbose flag.\n");
    printf("  -s <num>   Number of set index bits.\n");
    printf("  -E <num>   Number of lines per set.\n");
    printf("  -b <num>   Number of block offset bits.\n");
    printf("  -t <file>  Trace file.\n");
    printf("\nExamples:\n");
    printf("  %s -s 4 -E 1 -b 4 -t traces/yi.trace\n", argv[0]);
    printf("  %s -v -s 8 -E 2 -b 4 -t traces/yi.trace\n", argv[0]);
    exit(0);
}

// cache = sets * lines * blocks
cache build_cache (long long num_sets, int n, long long block_size) {

    cache newCache;
    cache_set set;
    line l;

    int setIndex;
    int li;

    newCache.sets = (cache_set*) malloc(sizeof(cache_set) * num_sets);

    for (setIndex = 0; setIndex < num_sets; setIndex++)
    {
        // create E lines
        set.lines =  (line *) malloc(sizeof(line) * n);
        newCache.sets[setIndex] = set;

        // create b blocks
        for (li = 0; li < n; li ++)
        {
            l.access_count = 0;
            l.valid = 0;
            l.tag = 0;
            set.lines[li] = l;
        }
    }

    return newCache;
}


int get_empty_line(cache_set set, var par) {

    line l;
    int n = par.E;
    int i;

    for (i = 0; i < n; i ++) {
        l = set.lines[i];
        if (l.valid == 0) {
            return i;
        }
    }

    return 0;
}

// get least recent used line
int get_LRU (cache_set this_set, var par, int *used_lines) {

    int n = par.E;

    int max_used = this_set.lines[0].access_count;
    int min_used = this_set.lines[0].access_count;

    int min_used_index = 0;

    int li;
    line l;

    for (li = 1; li < n; li ++) {

        l = this_set.lines[li];

        if (min_used > l.access_count) {
            min_used_index = li;
            min_used = l.access_count;
        }


        if (max_used < l.access_count) {
            max_used = l.access_count;
        }
    }

    used_lines[0] = min_used;
    used_lines[1] = max_used;

    return min_used_index;

}

var simulate_cache (cache this_cache, var par, mem_addr_t address) {

        int li;
        int cache_full = 1;

        int n = par.E;
        int prev_hit = par.hit;

        int tag_size = (64 - (par.s + par.b));

        unsigned long long temp = address << (tag_size);
        unsigned long long setIndex = temp >> (tag_size + par.b);

        mem_addr_t input_tag = address >> (par.s + par.b);

        cache_set query_set = this_cache.sets[setIndex];

        for (li = 0; li < n; li ++) {

            line l = query_set.lines[li];

            if (l.valid) {
                // cache hit
                if (l.tag == input_tag) {

                    l.access_count++;

                    par.hit++;

                    query_set.lines[li] = l;
                }

            } else if (!(l.valid) && (cache_full)) {
                cache_full = 0;
            }
        }
        // cache miss
        if (prev_hit == par.hit) {

            par.miss++;

        } else {
            return par;
        }

        int *used_lines = (int*) malloc(sizeof(int) * 2);
        int min_used_index = get_LRU(query_set, par, used_lines);
        // eviction
        if (cache_full) {

            par.evct++;

            query_set.lines[min_used_index].tag = input_tag;
            query_set.lines[min_used_index].access_count = used_lines[1] + 1;
        } else {

            int empty_line_index = get_empty_line(query_set, par);

            query_set.lines[empty_line_index].tag = input_tag;
            query_set.lines[empty_line_index].valid = 1;
            query_set.lines[empty_line_index].access_count = used_lines[1] + 1;
        }

        free(used_lines);
        return par;

}

long long bit_pow(int power) {
    long long result = 1;
    result = result << power;
    return result;
}

int main(int argc, char **argv)
{
    cache this_cache;
    var par;
    bzero(&par, sizeof(par));

    long long num_sets;
    long long block_size;

    FILE *read_trace;

    char cmd;
    mem_addr_t address;
    int size;

    char *trace_file;
    char c;
    while( (c=getopt(argc,argv,"s:E:b:t:vh")) != -1){
        switch(c){
        case 's':
            par.s = atoi(optarg);
            break;
        case 'E':
            par.E = atoi(optarg);
            break;
        case 'b':
            par.b = atoi(optarg);
            break;
        case 't':
            trace_file = optarg;
            break;
        case 'v':
            v_tag = 1;
            break;
        case 'h':
            printUsage(argv);
            exit(0);
        default:
            printUsage(argv);
            exit(1);
        }
    }
    if (par.s == 0 || par.E == 0 || par.b == 0 || trace_file == NULL) {
        printf("%s: Missing required command line argument\n", argv[0]);
        printUsage(argv);
        exit(1);
    }

    /* compute S and B based on information passed in; S = 2^s and B = 2^b */
    num_sets = pow(2.0, par.s);
    block_size = bit_pow(par.b);
    par.hit = 0;
    par.miss = 0;
    par.evct = 0;

    this_cache = build_cache(num_sets, par.E, block_size); /* build_cache takes as input sets, lines, and blocks */

    read_trace = fopen(trace_file,"r");

    /* rest of simulator routine reads commands in */
   	if (read_trace != NULL) {
        while (fscanf(read_trace, " %c %llx,%d", &cmd, &address, &size) == 3) {
            switch(cmd) {
                case 'I':
                break;
                case 'L':
                    par = simulate_cache(this_cache, par, address);
                break;
                case 'S':
                    par = simulate_cache(this_cache, par, address);
                break;
                case 'M':
                    par = simulate_cache(this_cache, par, address);
                    par = simulate_cache(this_cache, par, address);
                break;
                default:
                break;
            }
        }
    }

    printSummary(par.hit, par.miss, par.evct);

    fclose(read_trace);

    return 0;
}
