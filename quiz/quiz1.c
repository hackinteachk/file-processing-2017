// Nuttapat Koonarangsri
// 5881043

#include<stdio.h>

void prefix_sum(int *a, int *b, int *c){
    int temp = *b;
    *b = *a+*b;
    *c = *a+temp+*c;
}

void reverse(int *a, int n){
    int ans[n];
    int ans_i = 0;
    for(int i=n-1;i>=0;i--){
        ans[ans_i++] = a[i];
    }
    for(int i=0;i<n;i++){
        a[i] = ans[i];
    }
}

int main(){
    int a = 1;
    int b = 2;
    int c = 3;
    printf("a=%d b=%d c=%d\n",a,b,c);
    printf("Prefix Summing...\n");
    prefix_sum(&a,&b,&c);
    printf("a=%d b=%d c=%d\n",a,b,c);

    printf("Reversing the Array...\n");
    int n = 5;
    int ar[n];
    for(int i=0;i<n;i++){
        ar[i] = i+1;
    }
    printf("Original Array\n[ ");
    for(int i=0;i<n;i++){
        printf("%d ",ar[i]);
    }
    printf(" ]\n");
    reverse(ar,n);

    printf("Reversed Array\n[ ");
    for(int i=0;i<n;i++){
        printf("%d ",ar[i]);
    }
    printf(" ]\n");
}

