#include <stdio.h>
#include <math.h>

int isPowerOfTwo(long x);
int main(){
    long N;
    scanf("%lu",&N);
    long end = 2*N;

    while(N++ < end){
        // printf("n: %lu\n",N);
        if(isPowerOfTwo(N+1)){
            printf("%lu\n",N+1);
        }
    }
    return 0;
}

int isPowerOfTwo(long x){
    double n = log2((double)x);
    if((n-(int)n) > 0){
        return 0;
    }
    return 1;
}
