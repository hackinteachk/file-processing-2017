#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){
    if(argc != 2){
        printf("Usage ./len_hist <input>\n");
        return 1;
    }

    char *word = (char*)malloc(sizeof(char));
    int *count = (int*)calloc(16,sizeof(int));

    FILE *f = fopen(argv[1],"r");

    int max = 0;
    // count number of words in the file
    while(fgets(word,17,f)){
        int n = strlen(word)-1; //exclude '\n'
        if(n<2){break;}
        count[n]++;
        if(count[n] > max) max = count[n];
    }
    fclose(f);

    int rank = 1;
    while(max > 0){
        for(int i=16;i>0;i--){
            if(count[i] == max){
                printf("Rank %d: length %d sees %d word(s).\n",rank++,i,count[i]);
            }
        }
        max--;
    }


    return 0;
}
