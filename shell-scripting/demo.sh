#!/bin/bash

#no space
A="this is A"
# use $ to point to variable
echo $A

# '#' = number of arguments
echo $#." arguments inserted"

# 1 2 3 4...n are arg[n]
echo "first arg is ".$1
echo $@

#Comparing '' and ""
B='hello $A'
C="hello $A"
echo $B
echo $C

# use backtick ` ` to evaluate the command
P=`pwd`
echo `echo $A`
echo $(echo $A)
echo "current path is ".$P

# ? = exit code of the last command
echo "exit code : ".$?

#exit code
# 0 if true
# 1 else
# test $1 = "nuttapat"

if [ $1 = "nuttapat" ]
then
    echo "Access Granted for root"
elif [ $1 -eq "123" ]
then
    echo "Access Granted for 123"
else
    echo "Access Denied"
fi

for var in `ls`
do
    echo $var
done
