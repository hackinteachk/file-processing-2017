cat data.csv | cut -d',' -f1
cat data.csv | cut -d',' -f2,3

#translate uppercase to lowercase
cat data.csv | tr "A-Z" "a-z"

# might cause to DATA LOST !!!
#cat data.csv | tr "A-Z" "a-z" > data.csv
