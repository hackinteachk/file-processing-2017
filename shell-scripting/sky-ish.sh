# -10 = 10 lines before and after result(s)
cat sky.html | grep -10 "Sunsern"

# save the url to file named sky.html
#curl https://sky.muic.mahidol.ac.th/public/open_sections > sky.html

# show first/last 3 lines in sky.html
head -3 sky.html
tail -3 sky.html
