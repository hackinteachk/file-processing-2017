#!/bin/bash

#Take in a path and list out file name that start with an 'a'
path=$1
for f in `ls $1a*`
do
    if [ -f $f ]
    then
        echo `basename $f`
    fi
done
