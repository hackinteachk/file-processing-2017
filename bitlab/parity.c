#include <stdio.h>

int parity_check(int x){
    int x1 = (x>>16); //shift x 16 bits to the right (split x and x1 to be half-half)
    x = x^x1; // x = x XOR x1

    int x2 = (x>>8);
    x = x^x2;

    int x3 = (x>>4);
    x = x^x3;

    int x4 = (x>>2);
    x = x^x4;

    int x5 = (x>>1);
    x = x^x5;

    printf("%d\n",x&1);
    return x&1; // compare with least significant bit.
}

int main(){
    parity_check(-1);
    parity_check(0);
    parity_check(1);
    parity_check(2);
    parity_check(3);
    parity_check(4);
}
