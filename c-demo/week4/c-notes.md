#Compiling
    ```c
    gcc <File name>
	gcc -o <Output filename> <File to compile>
    ```

#Run
	```c
    ./<Output filename>
    ```

Integer Types

1. char			(8 bits long)
2. short 		(16 bits long)
3. int 			(32 bits long)
4. long			(≥ bits long)
5. long long	(64 bits)

#Printing

```c
printf( FORMAT_STRING, args);
```

###e.g.
	`printf("Name: %s Number:%d Price:%2.3f\n","Nut", 5881043, 2.345);`