#include<stdio.h>

void bad_swap(int x, int y){
    int temp = x;
    x = y;
    t = x;
}

void swap(int *x, int *y){
    int temp = *x;
    *x = *y;
    y = temp;
}

int main(){
    // each variable can asked for 1.Address 2.Value
    int a;
    // b is pointer of type int
    int *b;

    b = &a;
    a = 5;
    printf("a=%d *b=%d\n",a,*b);
    printf("address of a: %p\n address of b : %p\ncontent of b : %p\n", &a,&b,b);

    *b = 10; // a = 10
    printf("%d %d\n",a,*b);

    a = 20;
    printf("%d %d\n",a,*b);

    printf("%lu\n",sizeof(int));
    printf("%lu\n",sizeof(char));
    return 0;
    int u = 7;
    int v = 9;
    bad_swap(u,v);
    printf("u=%d v=%d",u,v);

    swap(&u,&v);
    printf("u=%d v=%d",u,v);
}
