#include <stdio.h>
#include <stdlib.h>


struct node_t{
    int* data;
    struct node_t *next;
};

typedef struct node_t node;

typedef struct {
    node *head;
    node *tail;
    int count;
}linkedlist;

int init_list(linkedlist **ll);
void print_list(linkedlist *ll);
void insert_front(linkedlist *ll, int *x);
void remove_front(linkedlist *ll,int **data);
void destroy_list(linkedlist *ll);

int main(){
    // node *head = (node*)malloc(sizeof(node));
    // node *head = (node*)malloc(sizeof(node));
    // node *n = (node*)malloc(sizeof(node));
    // head->next = ply;
    // ply->next = NULL;
    linkedlist *ll;
    int a=10;
    int b=20;
    int *x;

    init_list(&ll);
    print_list(ll);

    insert_front(ll,&a);
    print_list(ll);

    insert_front(ll,&b);
    print_list(ll);

    remove_front(ll,&x);
    printf("just remove %d\n",*x);
    print_list(ll);
}


// int init_list(linkedlist *ll){
//     // ll is NULL
//     if(!ll){
//         return 0;
//     }
//     ll->head = NULL;
//     ll->tail = NULL;
//     ll->count = 0;
//     return 1;
// }

int init_list(linkedlist **ll){
    linkedlist *tmp = (linkedlist*)malloc(sizeof(linkedlist));

    if(!*ll){
        return 0;
    }
    tmp->head = NULL;
    tmp->tail = NULL;
    tmp->count = 0;

    *ll = tmp;
    return 1;
}

void print_list(linkedlist *ll){
    node* index;
    // if index is null, break the loop
    for(index = ll->head; index; index=index->next){
        //*(index->data) get value of index.data (index.data = pointer)
        printf("%d ",*(index->data));
    }
    printf("\n");
}

void insert_front(linkedlist *ll, int *x){
    node *new = (node*)malloc(sizeof(node));
    new->data = x;
    new->next = ll->head;
    ll->head = new;
    ll->count = ll->count+1;
}

void remove_front(linkedlist *ll,int **data){
    if(!ll->head) return;
    node *target = ll->head;
    *data = target->data;
    ll->head = target->next;
    ll->count--;
    if(ll->head == NULL) ll->tail = NULL;
    free(target);
}
