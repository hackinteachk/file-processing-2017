//
// Created by poon. on 19/10/2017 AD.
//
#define L 1000000UL;
#define M 100000UL;
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//what is a linked list ???
//data structure that has 2 sides
//#include <stdio.h>
//#include <stdlib.h>
//#include <>
struct node_t{
    int * data;
    struct node_t *next;
};
typedef struct node_t node;
typedef struct {
    node *head;
    node *tail;
    int count;
}linkedlist;

int init_list(linkedlist *ll){
    if(ll == NULL)
        return 0;
    else {
        ll->head = NULL;
        ll->tail = NULL;
        ll->count = 0;
        return 1;
    }
}

void insert_front(linkedlist *ll, int *x){
    node *linku=(node*)malloc(sizeof(node));
    if(ll->tail == NULL) ll->tail = linku;
    linku->data =x;
    linku->next=ll->head;
    ll->head=linku;
    ll->count++;
}

unsigned long benchmark(void(*f)(),int iter){
    struct timeval begin,end;
    gettimeofday(&begin,NULL);
    for (int i = 0; i < iter; ++i) {
        (*f)();
    }
    gettimeofday(&end, NULL);
    return (1000000UL*(end.tv_sec - begin.tv_sec) + (end.tv_usec - begin.tv_usec))/iter;
}

void simple_loop(){
    linkedlist ll;
    init_list(&ll);
    for (long i = 0; i < 50000 ; ++i){
        insert_front(&ll,&i);
    }
}

void mem_copy(){
    int n = M;
    int *a = (int*)malloc(n* sizeof(int));
    int *b = (int*)malloc(n* sizeof(int));
    for (int i = 0; i <n ; ++i) {
        a[i] = b[i];
    }
    free(a);
    free(b);
}
void insert_front_memcpy(){
    int s[50000];
    for(int i=0;i<50000;i++){
        memmove(&s[1],&s[0],sizeof(int)*i);
        s[0] = 10;
    }
}

int main(){
    printf("insert_front loop time: %lu usec\n", benchmark(simple_loop,10));
    printf("insert_front_memcpy loop time: %lu usec\n", benchmark(insert_front_memcpy,10));
    return 0;

}