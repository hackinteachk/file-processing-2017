cmake_minimum_required(VERSION 3.8)
project(c_demo)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        "Lecture 9/demo.c"
        "Lecture 9/demo2.c"
        "Lecture 9/removeneg.c"
        "Lecture 9/revmalloc.c"
        linkedList-demo/linkedList.c
        week4/array.c
        week4/degreeConversion.c
        week4/helloworld.c
        week4/helloworld.i
        week4/pointer.c
        week4/printf.c
        "week5/file manipulation/file.c"
        "week5/file manipulation/wlc.c"
        week5/arrayDimension.c
        week5/file.c
        week5/struct.c
        arrayOfLinkedList.c
        benchmark.c
        countpos.c
        doublyLinkedList.c
        multiDArray.c)

include_directories(.)
include_directories("Lecture 9")
include_directories(linkedList-demo)
include_directories(week4)
include_directories(week5)
include_directories("week5/file manipulation")

add_executable(c_demo ${SOURCE_FILES})
add_executable(linkedlistBenchMark linkedlistBenchMark.c)