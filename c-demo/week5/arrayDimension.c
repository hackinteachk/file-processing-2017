#include<stdio.h>

int main(){
	int mat[4][4];
	int *p;

	//point p to address of mat[2][2]
	p = &mat[2][2];
	//assign value to localtion of p
	*p = 777;
	///mat[2][2] = 777

	p = *mat[0][0];
	*(p+5) = 888;
	//mat[1][1] = 888

	*(p+(i*4)+j) = 999;
	//mat[i][j]

	for(int i=0;i<4;i++){
		for(int j=0;j<4;j++){
			printf(" %p", &mat[i][j]);
		}
		printf("\n");
	}	
}