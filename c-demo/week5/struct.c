#include<stdio.h>

int main(){

	//struct -> pack variable into a block, organising
	struct fraction{
		int numerator;
		int denominator;
	} f1,f2;

	f1.numerator = 22;
	f1.denominator = 7;

	printf("%p, %p\n",&f1.numerator,&f1.denominator);
}