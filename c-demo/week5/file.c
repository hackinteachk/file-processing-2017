#include<stdio.h>
#include<stdlib.h>

int main(int argc, char* argv[]){

	if(argc < 3){
		printf("Error, argument not found\n");
		exit(-1);	
	}
	FILE *fin = fopen(argv[1], "r");
	FILE *fout= fopen(argv[2], "w");

	int running_sum = 0;
	int n=0;

	while(!feof(fin)){
		int point;
		fscanf(fin,"%d",&point);
		running_sum += point;
		n++;
	}

	fprintf(fout, "Average: %f\n",running_sum/(double)n );

	fclose(fin);
	fclose(fout);

}