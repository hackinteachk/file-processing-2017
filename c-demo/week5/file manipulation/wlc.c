#include<stdio.h>
#include<string.h>
#include<wctype.h>

//Count #words and #lines in the given input
int main(int argc, char* argv[]){
	int w_c = 0;
	int l_c = 0;
	if(argc < 3){
		//Use file input
		FILE *fin = fopen(argv[1], "r");
		FILE *fout= fopen(argv[2], "w");
		
		while(!feof(fin)){
			char c;
			fscanf(fin,"%c",&c);
			if(c=='\n'){
				l_c++;
			}
			if(iswspace(c)){
				w_c++;
			}
		}

		fprintf(fout,"Total Lines : %d\nTotal Words : %d\n",l_c,w_c);

		fclose(fin);
		fclose(fout);
	}else{
		char c = getchar();
		while(c != EOF){
			if(c=='\n'){
				l_c++;
			}
			if(iswspace(c)){
				w_c++;
			}
		}

		printf("Total Lines : %d\nTotal Words : %d\n",l_c,w_c);
		
		// char* name[10];
		// printf("Enter file name(<10 chars): ");
		// scanf("%s",*name);
		// FILE fout = fopen(name,"w");
		// fprintf(fout,"Total Lines : %d\nTotal Words : %d",l_c,w_c);
		// fclose(fout);
	}
	

}