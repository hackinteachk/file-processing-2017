#include <stdio.h>
#include <stdlib.h>

typedef struct node_t{
    void *data;
    struct node_t *next;
    struct node_t *prev;
} node;

#define n 20

int main(){
    // declare array of linkedlist
    // pointer of array,
    // node* is type of each elm in array 'A'
    // ,also allocate memory for array of nodes
    node* A[n];
    // allocate memory for each node.
    A[0] = (node*)malloc(sizeof(node));
    A[1] = NULL;
}
