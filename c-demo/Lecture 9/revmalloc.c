#include <stdio.h>
#include <stdlib.h>

void reverse(char *st, int n){

  char temp;
  for (int i = 0; i < n/2; i++) {
    /* code */
    temp = st[i];
    st[i] = st[n - i - 1];
    st[n - i - 1] = temp;
  }
}

int main(){
  int size;
  printf("size:");
  scanf("%d", &size);

  char* st = (char*) malloc(sizeof(char) * size);
  printf("str:");
  scanf("%s", st);

  reverse(st, size);
  printf("%d %s\n", size, st);

  free(st);
  st = NULL;

  return 0;
}
