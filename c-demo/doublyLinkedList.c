#include <stdio.h>
#include <stdlib.h>

typedef struct node_t{
    void *data;
    struct node_t *next;
    struct node_t *prev;
} node;

int main(){

}

void insert(node *current, node *newNode){
    newNode->next = current->next;
    newNode->prev = current;
    current->next = newNode;
    (newNode->next)->prev = newNode;
}

void remove(node *current){
    (current->prev)->next = current->next;
    (current->next)->prev = current->prev;
    free(current);
}
