#include <stdio.h>
#include <stdlib.h>

int main(){
    //create a[5][5];
    int **a =(int**)malloc(5*sizeof(int*));
    for(int i=0;i<5;i++){
        //can use array notation here
        a[i] = (int*)malloc(5*sizeof(int));
    }
    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            a[i][j] = i*10+j;
            printf("%02d ",a[i][j]);
        }
        printf("\n");
    }

    for(int i=0;i<5;i++){
        free(a[i]);
    }
    free(a);

}
